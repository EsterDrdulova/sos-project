# SOS-project

**Stustit firefox:**
`$ startx /usr/bin/firefox & exec i3`

**Zistiť aktuálnu veľkosť:**
`$ du / --exclude=/{proc,sys,dev} -abc | sort -n`

****Zoznam zmazaných súborov:****

**-zmazanie logovacích súborov**
`find /var/log -name "*.log*" -delete`

**-zmazanie súborov medzipamäti**
`rm -rfv /var/cache/*`

**-obnovenie databázy rpm**
`rpm --rebuilddb `

**-odstránenie knihovní z adresára usr/lib/**
`rm -rf /usr/lib/locale/* `
`rm -rf /usr/lib/jvm/* `
`rm -rf /usr/lib/python3.9 `
`rm -rf /usr/lib/firmware/* `


**-odstránenie knihovní z adresára var/lib/**
`rm -rf /var/lib/sss/*`
`rm -rf /var/lib/selinux/*`

**-odstránenie súborov:**
`rm -rf /usr/share/locale/* `
`rm -rf /usr/share/selinux/* `
`rm -rfv /usr/share/doc/  `
`rm -rfv /usr/share/man/`
`rm -rfv /usr/share/info/ `
`rm -rf /usr/share/adobe `
`rm -rf /usr/share/gnome`
`rm -rf /usr/share/eclipse `
`rm -rf /usr/share/games`
`rm -rf /usr/share/git-cores`
`rm -rf /usr/share/java`
`rm -rf /usr/share/javadoc `
`rm -rf /usr/share/javazi `
`rm -rf /usr/share/gstreamer-1.0 `
`rm -rf /usr/share/javazi-1.8`
`rm -rf /usr/share/jvm`
`rm -rf /usr/share/jvm-common/`
`rm -rf /usr/share/maven-metadata `
`rm -rf /usr/share/maven-poms `
`rm -rf /usr/share/qt5/`	
`rm -rf /usr/share/sssd`
`rm -rf /usr/share/sssd-kcm/ `
`cp -r /usr/share/locale/en_GB /opt/` 
`rm -rf /usr/share/locale/* `
`mv /opt/en_GB /ur/share/locale/ `

**-odstránenie cracklib**
`rpm -e --nodeps cracklib`
`sudo dnf -y remove iwl* cracklib-dicts`

**-ostránenie knihovní z adresára usr/lib64/**
`rm -rf/usr/lib64/python3.9 `
`rm -rf /usr/lib64/libQt5Gui.so.5.15.2`
`rm -rf /usr/lib64/libQt5WebKit.so.5.212.0`
`rm -rf /usr/lib64/qt5`
`rm -rf /usr/lib64/libpython3.9.so.1.0`
`rm -rf /usr/lib64/libQt5Network.so.5.15.2`




